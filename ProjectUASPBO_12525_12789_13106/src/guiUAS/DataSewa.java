package guiUAS;

import java.awt.EventQueue;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class DataSewa extends javax.swing.JFrame {


	Connection Con = S_Connection.getConnection();
    Statement st;
    ResultSet rs;
    PreparedStatement ps;
    int index;
    

	JFrame frame;
	private JTextField txtNamaPeminjam;
	private JTextField txtIdBuku;
	private JTable table;
	private JTable tb_info;
	private JTextField tfJudul;
	private JLabel txtTanggal;
	private JLabel txtJam;
	private JLabel txtBiaya;
	
	
	/**
	 * Launch the application.
	 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataSewa window = new DataSewa();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public DataSewa() {
		initialize();
		settingTable();
	}
	
//	Setting
	public void settingTable(){
	        txtTanggal.setText(getDateInfo());
	        txtJam.setText(getTime());
	        txtBiaya.setText("0");
	        showTable();
	    }
	    
	public void getBukuInfo() {
		try{
            String query = " SELECT * FROM buku where buku_id = '"+ txtIdBuku.getText() +"' ";
            st = Con.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {                
                tfJudul.setText(rs.getString("judul_buku"));
            }
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
	}
	
	public String getDateInfo()
    {
        Date d = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String theDate = dateFormat.format(d);
        return theDate;
    }
	 public String getTime()
	    {
	        Calendar cal = Calendar.getInstance();
	        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	        String theTime = timeFormat.format(cal.getTime());
	        return theTime;
	    }
	
	public void inputData(){
        try {
            String query = "INSERT INTO `sewabuku` (`id`, `nama_peminjam`, `buku_id`, `buku_judul`, `tanggal_pinjam`, `tanggal_harus_kembali`,`tanggal_kembali`, `denda`, `biaya_sewa`) VALUES (NULL, ?, ?, ?, ?, NULL, NULL,NULL, NULL)";
            
            ps = Con.prepareStatement(query);
            ps.setString(1, txtNamaPeminjam.getText());
            ps.setString(2, txtIdBuku.getText());
            ps.setString(3, tfJudul.getText());
            ps.setString(4, getDateInfo());
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Berhasil di Input!");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
        showTable();
    }
	
	public void showTable(){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("No");
		model.addColumn("Peminjam");
		model.addColumn("ID Buku");
		model.addColumn("Judul Buku");
		model.addColumn("Tanggal Pinjam");
		model.addColumn("Tanggal Deadline");
		model.addColumn("Tanggal Kembali");
		model.addColumn("Denda");
		model.addColumn("Total biaya");
		
        int i = 1;
        try{
            String query = "SELECT * FROM sewabuku";
            st = Con.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {                
                model.addRow(new Object[] {i++,
                		rs.getString("nama_peminjam"),
                		rs.getString("buku_id"),
                		rs.getString("buku_judul"),
                		rs.getString("tanggal_pinjam") ,
                		rs.getString("tanggal_harus_kembali"),
                		rs.getString("tanggal_kembali"),
                		rs.getString("denda" ), 
                		rs.getString("biaya_sewa")
                		});
            }
            tb_info.setModel(model);
			
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        
        tb_info.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtNamaPeminjam.setEditable(false);
		        int index = tb_info.getSelectedRow();
		        System.out.println(index);
		        editTable(index);
			}
		});
    }
	       
	
	public void editTable(int index){
        this.index = index;
        txtNamaPeminjam.setText(tb_info.getModel().getValueAt(index, 1).toString());
        txtIdBuku.setText(tb_info.getModel().getValueAt(index, 2).toString());
        tfJudul.setText(tb_info.getModel().getValueAt(index, 3).toString());
    }
	
	public void editData(){
        try{
            String query = "UPDATE `sewabuku` SET `judul_buku` ='"+tfJudul.getText()+"' WHERE `nama_peminjam` ='"+txtNamaPeminjam.getText()+"'";
            System.out.println("Query: " + query);
            ps = Con.prepareStatement(query);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data berhasil di update!");
        }catch(Exception e){
            System.out.println("Error: " + e.getMessage());
        }
        showTable();
    }
	 public void kembalikanBuku(){
	        String query = "UPDATE `sewabuku` SET `tanggal_kembali` = ?, `denda` = ?, `biaya_sewa` = ? WHERE `sewabuku`.`nama_peminjam` = '"+ txtNamaPeminjam.getText().toString() +"'";
	        try{
	            ps = Con.prepareStatement(query);
	            ps.setString(1, getDateInfo());
	            ps.setString(2, getDenda() + "");
	            ps.setString(3, (getDenda()+5000) + "");
	            ps.executeUpdate();
	            JOptionPane.showMessageDialog(null, "Buku berhasil dikembalikan!");
	        }catch(Exception e){
	            System.out.println("Error kembalikanBuku: " + e.getMessage());
	        }
	        showTable();
	    }
	 public int getDenda(){
	        txtBiaya.setText(((getRangeDate() * 2000)+ 5000) + "");	
	        return (getRangeDate() * 2000);
	    }
	 public int getRangeDate(){
	        ArrayList<LocalDate> totalDates = new ArrayList<>();
	        String startDate = null;
	        try{
	            String query = "SELECT tanggal_pinjam FROM sewabuku WHERE `sewabuku`.`nama_peminjam`='"+ txtNamaPeminjam.getText() +"'";
	            st = Con.createStatement();
	            rs = st.executeQuery(query);
	            if (rs.next()) {
	                startDate = rs.getString("tanggal_pinjam");
	            }
	        }catch(Exception e){
	            System.out.println("Error: " + e.getMessage());
	        }
	        String s = startDate;
	        String e = getDateInfo();
	        LocalDate start = LocalDate.parse(s);
	        LocalDate end = LocalDate.parse(e);
	        while (!start.isAfter(end)) {
	            totalDates.add(start);
	            start = start.plusDays(1);
	        }
	        
	        int temp = totalDates.size() - 1;
	        System.out.println("berapa hari terlambat: " + temp);
	        return temp;
	    }
	

	/**
	 * Create the application.
	 */


	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Persewaan Buku XYZ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNewLabel.setBounds(141, 10, 345, 47);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Nama Peminjam");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(40, 142, 104, 26);
		frame.getContentPane().add(lblNewLabel_2);
		
		txtNamaPeminjam = new JTextField();
		txtNamaPeminjam.setBounds(182, 139, 152, 28);
		frame.getContentPane().add(txtNamaPeminjam);
		txtNamaPeminjam.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Id Buku");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(40, 178, 104, 13);
		frame.getContentPane().add(lblNewLabel_3);
		
		txtIdBuku = new JTextField();
		txtIdBuku.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER) {
				      getBukuInfo();
				   }
			}
		});
		txtIdBuku.setBounds(182, 177, 152, 28);
		frame.getContentPane().add(txtIdBuku);
		txtIdBuku.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Biaya");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_4.setBounds(416, 191, 51, 26);
		frame.getContentPane().add(lblNewLabel_4);
		
		JButton btnNewButton = new JButton("Simpan");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inputData();
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton.setBounds(40, 254, 85, 26);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Edit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				editData();
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton_1.setBounds(162, 254, 85, 26);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Kembalikan Buku");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				kembalikanBuku();
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton_2.setBounds(285, 254, 129, 26);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Cetak Laporan");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					String sql = "SELECT * FROM sewabuku";

					JasperDesign jdesign = JRXmlLoader.load("C:\\Users\\user\\eclipse-workspace\\UAS_PBO\\projectuaspbo_12525_13106_12789\\ProjectUASPBO_12525_12789_13106\\res\\ReportDataSewaBuku.jrxml");

					JRDesignQuery updateQuery = new JRDesignQuery();
					updateQuery.setText(sql);
					jdesign.setQuery(updateQuery);
					
					JasperReport Jreport = JasperCompileManager.compileReport(jdesign);
					JasperPrint JasperPrint = JasperFillManager.fillReport(Jreport, null, Con);

					JasperViewer.viewReport(JasperPrint, false);
					
					ExportToExcel exportExc = new ExportToExcel(table, new File("Laporan Data Sewa Buku.xls"));
					
					
				} 
				catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		btnNewButton_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnNewButton_3.setBounds(452, 253, 144, 28);
		frame.getContentPane().add(btnNewButton_3);
		
		table = new JTable();
		table.setFont(new Font("Tahoma", Font.PLAIN, 12));
		table.setBounds(40, 409, 0, 0);
		frame.getContentPane().add(table);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(50, 295, 520, 114);
		frame.getContentPane().add(scrollPane);
		
		tb_info = new JTable();
		scrollPane.setViewportView(tb_info);
		tb_info.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JLabel lblNewLabel_1 = new JLabel("Tanggal");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_1.setBounds(42, 99, 56, 26);
		frame.getContentPane().add(lblNewLabel_1);
		
		txtTanggal = new javax.swing.JLabel();
		txtTanggal.setBounds(104, 103, -3, 22);
		frame.getContentPane().add(txtTanggal);
		
		JLabel lblNewLabel_6 = new JLabel("New label");
		lblNewLabel_6.setBounds(111, 108, 0, 13);
		frame.getContentPane().add(lblNewLabel_6);
		
		JLabel lblNewLabel_5 = new JLabel("Jam");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_5.setBounds(416, 103, 51, 19);
		frame.getContentPane().add(lblNewLabel_5);
		
		txtJam = new javax.swing.JLabel();
		txtJam.setBounds(452, 97, 12, 28);
		frame.getContentPane().add(txtJam);
		
		JLabel lblNewLabel_7 = new JLabel("Rp");
		lblNewLabel_7.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_7.setBounds(455, 195, 62, 19);
		frame.getContentPane().add(lblNewLabel_7);
		
		txtBiaya = new javax.swing.JLabel();
		txtBiaya.setBounds(488, 200, 28, 13);
		frame.getContentPane().add(txtBiaya);
		
		JLabel lblNewLabel_8 = new JLabel("Judul Buku");
		lblNewLabel_8.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_8.setBounds(40, 218, 85, 13);
		frame.getContentPane().add(lblNewLabel_8);
		
		tfJudul = new JTextField();
		tfJudul.setBounds(182, 218, 152, 26);
		frame.getContentPane().add(tfJudul);
		tfJudul.setColumns(10);
	}
	public JTable getTable_1() {
		return tb_info;
	}
}
