package guiUAS;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.SystemColor;

public class dashboard {

	JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					dashboard window = new dashboard();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public dashboard() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel menuDataBuku = new JPanel();
		menuDataBuku.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
//				FormBuku form = new FormBuku();
//				FormBuku.main(null);
				FormBuku fb = new FormBuku();
	        	fb.frame.setVisible(true);
	        	frame.dispose();
			}
		});
		menuDataBuku.setBackground(SystemColor.desktop);
		menuDataBuku.setBounds(60, 169, 245, 110);
		frame.getContentPane().add(menuDataBuku);
		menuDataBuku.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Data Buku");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel.setBounds(67, 27, 128, 59);
		menuDataBuku.add(lblNewLabel);
		
		JPanel menuDataSewa = new JPanel();
		menuDataSewa.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
//				DataSewa sewa = new DataSewa();
//				DataSewa.main(null);
				DataSewa fb = new DataSewa();
	        	fb.frame.setVisible(true);
	        	frame.dispose();
			}
		});
		menuDataSewa.setBackground(SystemColor.desktop);
		menuDataSewa.setBounds(315, 169, 245, 110);
		frame.getContentPane().add(menuDataSewa);
		menuDataSewa.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("Data Sewa");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setBounds(66, 41, 113, 29);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		menuDataSewa.add(lblNewLabel_1);
		
		JPanel btnLogout = new JPanel();
		btnLogout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
//				login login = new login();
//	        	login.main(null);
				login login = new login();
	        	login.frmLogin.setVisible(true);
	        	frame.dispose();
			}
		});
		btnLogout.setBackground(UIManager.getColor("Button.focus"));
		btnLogout.setBounds(473, 387, 111, 34);
		frame.getContentPane().add(btnLogout);
		btnLogout.setLayout(null);
		
		JLabel lblNewLabel_2 = new JLabel("LOG OUT");
		lblNewLabel_2.setBounds(33, 11, 51, 14);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setBackground(new Color(255, 255, 255));
		btnLogout.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Sistem Peminjaman Buku");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(199, 57, 205, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("\"Perpustakaan Nakula\"");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(164, 69, 267, 33);
		frame.getContentPane().add(lblNewLabel_4);
	}

}
