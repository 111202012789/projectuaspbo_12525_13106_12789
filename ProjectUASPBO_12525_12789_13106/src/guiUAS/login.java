package guiUAS;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import java.awt.SystemColor;
import javax.swing.UIManager;

public class login {

	JFrame frmLogin;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					login window = new login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.getContentPane().setBackground(Color.WHITE);
		frmLogin.setTitle("Login Admin");
		frmLogin.setBounds(100, 100, 640, 480);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(SystemColor.desktop);
		panel.setForeground(new Color(204, 255, 255));
		panel.setBounds(177, 105, 274, 249);
		frmLogin.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("LOGIN");
		lblNewLabel.setForeground(new Color(248, 248, 255));
		lblNewLabel.setBounds(110, 31, 58, 21);
		panel.add(lblNewLabel);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		txtPassword = new JPasswordField();
		txtPassword.setBounds(42, 134, 191, 28);
		panel.add(txtPassword);
		
		JLabel lblNewLabel_1 = new JLabel("Username");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setBounds(42, 73, 77, 14);
		panel.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Password");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setBounds(42, 120, 89, 14);
		panel.add(lblNewLabel_2);
		
		txtUsername = new JTextField();
		txtUsername.setBounds(42, 87, 191, 28);
		panel.add(txtUsername);
		txtUsername.setColumns(10);
		
		JButton btnLogin = new JButton("LOG IN");
		btnLogin.setBackground(Color.BLACK);
		btnLogin.setForeground(UIManager.getColor("CheckBox.shadow"));
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		        if (txtUsername.getText().equals("")){
		            JOptionPane.showMessageDialog(null, "Mohon isi form username");
		            txtUsername.requestFocus();
		        }
		        else if(txtPassword.getText().equals("")){
		            JOptionPane.showMessageDialog(null, "Mohon isi form password");
		            txtPassword.requestFocus();
		        }
		        else if (txtUsername.getText().contains("admin123")&&txtPassword.getText().contains("admin123")){
//		            new dashboard().show();
//		            this.dispose();
		        	dashboard dash = new dashboard();
//		        	dashboard.main(null);
		        	dash.frame.setVisible(true);
		        	frmLogin.dispose();
		        	
		        	
		        }
		        else {
		            JOptionPane.showMessageDialog(null,"Maaf,Username atau password tidak sesuai");
		        }
			}
		});
		btnLogin.setBounds(42, 189, 191, 35);
		panel.add(btnLogin);
		
		JLabel lblNewLabel_3 = new JLabel("Selamat Datang!");
		lblNewLabel_3.setForeground(new Color(105, 105, 105));
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(212, 52, 209, 23);
		frmLogin.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Sistem Peminjaman Buku Perpustakaan Nakula");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(177, 76, 274, 14);
		frmLogin.getContentPane().add(lblNewLabel_4);
	}
}
